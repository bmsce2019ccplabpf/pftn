#include<stdio.h>
int swap(int*m,int*n);
int main()
 {
   int m,n;
   printf("m and n\n");
   scanf("%d%d",&m,&n);
   printf("the values of m and n before swapping are:\n");
   printf("m=%d,n=%d\n",m,n);
   swap(&m,&n);
   printf("the values of m and n after swapping are:\n");
   printf("m=%d,n=%d\n",m,n);
   return 0;
 }
int swap(int*m,int*n)
 {
   int temp;
   temp=*m;
   *m=*n;
   *n=temp;
   return 0;
 }