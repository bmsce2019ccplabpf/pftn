#include<stdio.h>
int input()
{
    int a;
    printf("Enter an integer value\n");
    scanf("%d",&a);
    return a;
}
int compute(int a)
{
    int temp,add=0;
    while(a!=0)
    {
        temp = a%10;
        add = add + temp;
        a = a/10;
    }
    return add;
}
void output(int add)
{
    printf("sum of digits = %d",add);
}
void main()
{
    int a,add;
    a = input();
    add = compute(a);
    output(add);
}